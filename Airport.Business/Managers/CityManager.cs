﻿using System.Linq;
using Airport.Core.Interfaces.Managers;
using Airport.Core.Interfaces.Repositories;
using Airport.Core.Interfaces.UnitOfWork;
using Airport.Core.Models;

namespace Airport.Business.Managers
{
    public class CityManager : ICityManager
    {
        private readonly ICityRepository _cityRepository;
        private readonly IUnitOfWork _unitOfWork;

        public CityManager(IUnitOfWork unitOfWork,
                              ICityRepository cityRepository)
        {
	        _cityRepository = cityRepository;
            _unitOfWork = unitOfWork;
        }

        public City[] GetAllCities()
        {
            return _cityRepository.SelectAll().ToArray();
        }

        public City GetCityById(int cityId)
        {
            return _cityRepository.Find(cityId);
        }

        public void AddCity(string cityName)
        {
            var city = new City
            {
                CityName = cityName,
            };

            _cityRepository.Insert(city);
            _unitOfWork.Save();
        }

        public void EditCity(int cityId, string cityName)
        {
            var record = _cityRepository.Get(cityId);
            if (record != null)
            {
                record.CityName = cityName;
                _unitOfWork.Save();
            }
        }
        public void DeleteCity(int cityId)
        {
            var record = _cityRepository.Find(cityId);
            if (record != null)
            {
                _cityRepository.Delete(record);
                _unitOfWork.Save();
            }
        }

    }
}
