﻿using System.Linq;
using Airport.Core.Interfaces.Managers;
using Airport.Core.Interfaces.Repositories;
using Airport.Core.Interfaces.UnitOfWork;
using Airport.Core.Models;

namespace Airport.Business.Managers
{
    public class CompanyManager : ICompanyManager
    {
        private readonly ICompanyRepository _companyRepository;
        private readonly IUnitOfWork _unitOfWork;

        public CompanyManager(IUnitOfWork unitOfWork,
                              ICompanyRepository companyRepository)
        {
            _companyRepository = companyRepository;
            _unitOfWork = unitOfWork;
        }

        public Company[] GetAllCompanies()
        {
            return _companyRepository.SelectAll().ToArray();
        }

        public Company GetCompanyById(int companyId)
        {
            return _companyRepository.Find(companyId);
        }

        public void AddCompany(string companyName, int cityId)
        {
            var company = new Company
            {
                CompanyName = companyName,
                CityId = cityId
            };

            _companyRepository.Insert(company);
            _unitOfWork.Save();
        }

        public void EditCompany(int companyId, string companyName, int companyCityId)
        {
            var record = _companyRepository.Get(companyId);
            if (record != null)
            {
                record.CompanyName = companyName;
                record.CityId = companyCityId;

                _unitOfWork.Save();
            }
        }
        public void DeleteCompany(int companyId)
        {
            var record = _companyRepository.Find(companyId);
            if (record != null)
            {
                _companyRepository.Delete(record);
                _unitOfWork.Save();
            }
        }
    }
}
