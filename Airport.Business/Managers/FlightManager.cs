﻿using System;
using System.Linq;
using Airport.Core.Enums;
using Airport.Core.Interfaces.Managers;
using Airport.Core.Interfaces.Repositories;
using Airport.Core.Interfaces.UnitOfWork;
using Airport.Core.Models;

namespace Airport.Business.Managers
{
	public class FlightManager : IFlightManager
	{
		private readonly IFlightRepository _flightRepository;
		private readonly IUnitOfWork _unitOfWork;

		public FlightManager(IUnitOfWork unitOfWork,
							  IFlightRepository flightRepository)
		{
			_flightRepository = flightRepository;
			_unitOfWork = unitOfWork;
		}

		public Flight[] GetAllFlights()
		{
			return _flightRepository.SelectAll().ToArray();
		}
		public Flight GetFlightById(int flightId)
		{
			return _flightRepository.Find(flightId);
		}

		public void AddFlight(string flightNumber, DateTime flightDateTime, TimeSpan flightDuration, int flightStatusId,
							  int cityFromId, int cityToId, int companyId, int terminalId)
		{
			var flight = new Flight
			{
				FlightNumber = flightNumber,
				FlightDateTime = flightDateTime,
				FlightDuration = flightDuration,
				FlightStatusId = flightStatusId,
				CityFromId = cityFromId,
				CityToId = cityToId,
				CompanyId = companyId,
				TerminalId = terminalId
			};

			_flightRepository.Insert(flight);
			_unitOfWork.Save();
		}

		public void EditFlight(int flightId, string flightNumber, DateTime flightDateTime, TimeSpan flightDuration, int flightStatusId,

								int cityFromId, int cityToId, int companyId, int terminalId)
		{
			var record = _flightRepository.Get(flightId);
			if (record != null)
			{
				record.FlightNumber = flightNumber;
				record.FlightDateTime = flightDateTime;
				record.FlightDuration = flightDuration;
				record.FlightStatusId = flightStatusId;
				record.CityFromId = cityFromId;
				record.CityToId = cityToId;
				record.CompanyId = companyId;
				record.TerminalId = terminalId;

				_unitOfWork.Save();
			}
		}
		public void DeleteFlight(int flightId)
		{
			var record = _flightRepository.Find(flightId);
			if (record != null)
			{
				_flightRepository.Delete(record);
				_unitOfWork.Save();
			}
		}
	}
}
