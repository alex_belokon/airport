﻿using Airport.Core;
using Airport.Core.Interfaces;
using Airport.Core.Structures;
using Airport.Core.FlightEventArgs;
using System;
using System.Collections.Generic;
using Airport.Core.BusinnessModels;
using Airport.Data;

namespace Airport.Business.Managers
{
    public class PrintManager : IPrintManager
    {
        private TableStructure FlightTableStructure = GetTable();
        private int number = 0;
        // Header row for Tables
        readonly static string[] headerRow = { "Time", "Flight", "Carrier", "Destination", "Terminal", "Gate", "Status" };

        // Print arrival and departure Table
        public void Print(IList<IFlight> flights)
        {
            if (flights == null || flights.Count == 0)
            {
                Console.WriteLine("\n\t\t\t\t\tTable of flights empty");
                return;
            }
            //Upper border
            Console.WriteLine(new String('_', 118));
            PrintTableRow(headerRow, ConsoleColor.White);
            Console.WriteLine(new string('-', 118));
            string[] flightRow = new string[headerRow.Length];
            foreach (var flight in flights)
            {
                var textColor = ConsoleColor.White;
                textColor = flight is Arrival ? textColor = ConsoleColor.Cyan : ConsoleColor.Yellow;
                flightRow[0] = flight.Time.ToShortTimeString() + " " + flight.Time.Day.ToString() + "." + flight.Time.Month.ToString() + ".17 ";
                flightRow[1] = flight.Number.ToString();
                flightRow[2] = flight.AirlineCompany.ToString().Replace('_', ' ');
                flightRow[3] = flight.City;
                flightRow[4] = flight.TerminalName.ToString();
                flightRow[5] = flight.TerminalGate.ToString();
                flightRow[6] = flight.FlightStatus.ToString().Replace('_', ' ');
                PrintTableRow(flightRow, textColor);
            }
            // Closed border
            Console.WriteLine(new string('_', 118) + "\n");
        }
        public void Print()
        {
            //Upper border
            Console.WriteLine("\n" + new string('_', 118) + "\n");

            foreach (var cell in FlightTableStructure.Header) { Console.Write(cell); }
            Console.WriteLine(new string('-', 118) + "\n");
            foreach (var row in FlightTableStructure.Rows)
            {
                foreach (var cell in row.Cells) { Console.Write(cell); }
            }
            // Closed border
            Console.WriteLine(new string('_', 118) + "\n");
        }

        public void ChangeTableStructure()
        {
            FlightTableStructure = GetTable();
        }
        //print a row
        private void PrintTableRow(string[] row, ConsoleColor textColor)
        {
            Console.ForegroundColor = textColor;
            foreach (var cell in row)
            {
                if (cell.Length >= 17)
                {
                    Console.Write("{0, -16}|", cell.Substring(0, 13) + "...");
                }

                else
                {
                    Console.Write("{0, -16}|", cell);
                }
            }
            Console.ResetColor();
            Console.WriteLine();
        }

        public void PrintMenu()
        {
            Console.WriteLine("\n\t\t\t\t\t\tMenu: ");
            Console.WriteLine("1. Edit Flight by Number");
            Console.WriteLine("2. Search Flights in Table by column names");
            Console.WriteLine("3. Search of the flight which is the nearest (1 hour) to the specified time to/from the specified port");
            Console.WriteLine("4. Add Flight");
            Console.WriteLine("5. Delete by Number");
            Console.WriteLine("6. Exit");
        }

        //1. Edit Flight by Number
        private void PrintSubmenu1()
        {
            Console.WriteLine("\n\t\t\t\tPlease select the parameter you want to change ");
            foreach (var item in headerRow)
            {
                Console.WriteLine(++number + ". " + item);
            }
            number = 0;
        }

        //2. Search Flights in Table by column names
        private void PrintSubmenu2()
        {
            Console.WriteLine("\n\t\t\tPlease enter the parameter for which the flight will be searched");
            foreach (var item in headerRow)
            {
                Console.WriteLine(++number + ". " + item);
            }
            number = 0;
        }

        //3. Search Flights in Table by time and city
        private void PrintSubmenu3()
        {

        }

        //4. Add Flight
        private void PrintSubmenu4()
        {
            Console.WriteLine("\nPlease enter Flight Type");
            Console.WriteLine("1. Arrival");
            Console.WriteLine("2. Departure");
        }

        public void PrintSubmenu(int number)
        {
            switch (number)
            {
                case 1:
                    PrintSubmenu1();
                    break;
                case 2:
                    PrintSubmenu2();
                    break;
                case 3:
                    PrintSubmenu3();
                    break;
                case 4:
                    PrintSubmenu4();
                    break;
                default:
                    break;
            }
        }

        public static TableStructure GetTable()
        {

            int index = -1;
            //create structure
            TableStructure FlightTS = new TableStructure
            {
                Header = new string[headerRow.Length],
                Rows = new TableRow[MemoryStorage.Flights.Count]

            };
            // fill header
            for (int i = 0; i < headerRow.Length; i++)
            {
                string headerCell = headerRow[i];
                if (headerCell.Length >= 17)
                {
                    FlightTS.Header[i] += headerCell.Substring(0, 13) + "...|";
                }
                else
                {
                    FlightTS.Header[i] += headerCell + new string(' ', 16 - headerCell.Length) + "|";
                }
            }
            foreach (var item in MemoryStorage.Flights)
            {
                ++index;
                string[] rowCells = new string[]{
                item.Time.ToShortTimeString() + " " +item.Time.Day.ToString() + "." + item.Time.Month.ToString() + ".17 ",
                item.Number.ToString(),
                item.AirlineCompany.ToString().Replace('_', ' '),
                item.City,
                item.TerminalName.ToString(),
                item.TerminalGate.ToString(),
                item.FlightStatus.ToString().Replace('_', ' ')
                };
                FlightTS.Rows[index].Cells = CellTextCutter(rowCells);
            }
            return FlightTS;
        }

        private static string[] CellTextCutter(string[] cells)
        {
            string[] cellsWithCuttedText = new string[cells.Length];
            for (int i = 0; i < cells.Length; i++)
            {
                if (cells[i].Length >= 17)
                {
                    cellsWithCuttedText[i] = cells[i].Substring(0, 13) + "...|";
                }
                else
                {
                    cellsWithCuttedText[i] = cells[i] + new string(' ', 16 - cells[i].Length) + "|";
                }
            }
            return cellsWithCuttedText;

        }
    }
}
