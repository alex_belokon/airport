﻿using Airport.Core;
using Airport.Core.Interfaces;
using Airport.Core.Enums;
using Airport.Core.FlightEventArgs;
using System;
using System.Collections.Generic;
using System.Linq;
using Airport.Business.Managers;
using Airport.Core.BusinnessModels;
using Airport.Data;

namespace Airport.Business
{
    public class SimpleFlightManager : IFlightManager
    {
        public delegate void FlightCountChangeHandler(object sender, FlightEventArgs e);
        public event FlightCountChangeHandler FlightCountChangeEvent;
        public EventArgs arg = new EventArgs();
        private int flightcount = 0 ;
        public int Flightount
        {
            get
            {
                return flightcount;
            }
            set
            {

            }
        }
        public List<Arrival> GetArrivals()
        {
            Arrival arrival1 = new Arrival
            {
                FlightType = FlightTypeEnum.Arrived,
                FlightStatus = FlightStatusEnum.Gate_Closed,
                TerminalName = TerminalNameEnum.A,
                AirlineCompany = AirlineCompanyEnum.Air_Baltic,
                City = "Riga",
                TerminalGate = TerminalGateEnum.First,
                Number = 1234,
                Time = DateTime.Now
            };
            //Arrival arrival2 = new Arrival
            //{
            //    FlightType = FlightTypeEnum.Arrived,
            //    FlightStatus = FlightStatusEnum.Unknown,
            //    TerminalName = TerminalNameEnum.B,
            //    AirlineCompany = AirlineCompanyEnum.Air_France,
            //    City = "Paris",
            //    TerminalGate = TerminalGateEnum.Second,
            //    Number = 5678,
            //    Time = DateTime.Now.AddHours(-2)
            //};
            //Arrival arrival3 = new Arrival
            //{
            //    FlightType = FlightTypeEnum.Arrived,
            //    FlightStatus = FlightStatusEnum.Excpected,
            //    TerminalName = TerminalNameEnum.C,
            //    AirlineCompany = AirlineCompanyEnum.Atlasjet_Ukraine,
            //    City = "Kiev",
            //    TerminalGate = TerminalGateEnum.Third,
            //    Number = 4321,
            //    Time = DateTime.Now.AddHours(3)
            //};
            //Arrival arrival4 = new Arrival
            //{
            //    FlightType = FlightTypeEnum.Arrived,
            //    FlightStatus = FlightStatusEnum.Arrived,
            //    TerminalName = TerminalNameEnum.C,
            //    AirlineCompany = AirlineCompanyEnum.Turkish_Airlines,
            //    City = "Sharm El Sheikh",
            //    TerminalGate = TerminalGateEnum.Second,
            //    Number = 8765,
            //    Time = DateTime.Now.AddHours(-6)
            //};
            List<Arrival> ArrivalList = new List<Arrival>();
            ArrivalList.Add(arrival1);
            //ArrivalList.Add(arrival2);
            //ArrivalList.Add(arrival3);
            //ArrivalList.Add(arrival4);
            return ArrivalList;
        }
        public List<Departure> GetDepartures()
        {
            Departure departure1 = new Departure
            {
                FlightType = FlightTypeEnum.Departed,
                FlightStatus = FlightStatusEnum.Delayed,
                TerminalName = TerminalNameEnum.A,
                AirlineCompany = AirlineCompanyEnum.Lufthansa,
                City = "Berlin",
                TerminalGate = TerminalGateEnum.First,
                Number = 9630,
                Time = DateTime.Now.AddHours(-5)
            };
            //Departure departure2 = new Departure
            //{
            //    FlightType = FlightTypeEnum.Departed,
            //    FlightStatus = FlightStatusEnum.CheckIn,
            //    TerminalName = TerminalNameEnum.B,
            //    AirlineCompany = AirlineCompanyEnum.Turkish_Airlines,
            //    City = "Odessa",
            //    TerminalGate = TerminalGateEnum.Second,
            //    Number = 8520,
            //    Time = DateTime.Now.AddHours(3)
            //};
            //Departure departure3 = new Departure
            //{
            //    FlightType = FlightTypeEnum.Departed,
            //    FlightStatus = FlightStatusEnum.Unknown,
            //    TerminalName = TerminalNameEnum.C,
            //    AirlineCompany = AirlineCompanyEnum.Azerbaijan_Airlines,
            //    City = "Minsk",
            //    TerminalGate = TerminalGateEnum.First,
            //    Number = 9856,
            //    Time = DateTime.Now.AddHours(-4)
            //};
            //Departure departure4 = new Departure
            //{
            //    FlightType = FlightTypeEnum.Departed,
            //    FlightStatus = FlightStatusEnum.Gate_Closed,
            //    TerminalName = TerminalNameEnum.A,
            //    AirlineCompany = AirlineCompanyEnum.Egypt_Air,
            //    City = "Sharm El Sheikh",
            //    TerminalGate = TerminalGateEnum.First,
            //    Number = 7410,
            //    Time = DateTime.Now.AddHours(-5)
            //};
            List<Departure> DepartureList = new List<Departure>();
            DepartureList.Add(departure1);
            //DepartureList.Add(departure2);
            //DepartureList.Add(departure3);
            //DepartureList.Add(departure4);
            return DepartureList;
        }
        public IFlight[] SearchFlightByParameter(int searchParameter, IFlight[] flights)
        {

            IFlight[] foundFlights = null;
            //IFlight[] tempFlight = null;
            if (flights == null || flights.Count() == 0)
            {
                Console.WriteLine("\n\t\t\t\t\tTable of flights empty");
                return foundFlights;
            }


            string input = String.Empty;

            switch (searchParameter)
            {
                case 1:
                    //Search by Time
                    Console.WriteLine("Enter the time to search for the flight in the format yyyy-mm-dd hh:mm");
                    //Example "2017-10-11 20:26";
                    input = Console.ReadLine();
                    DateTime.TryParse(input, out DateTime dataTimeSearch);
                    #region timeSearchOldVersion
                    //foreach (var flight in flights)
                    //{
                    //    if (flight.Time.ToShortTimeString() == searchedTime && foundFlights != null)
                    //    {
                    //        tempFlight = new IFlight[foundFlights.Length + 1];
                    //        Array.Copy(foundFlights, tempFlight, foundFlights.Length);
                    //        tempFlight[tempFlight.Length - 1] = flight;
                    //        foundFlights = tempFlight;
                    //    }
                    //    if (flight.Time.ToShortTimeString() == searchedTime && foundFlights == null)
                    //    {
                    //        foundFlights = new IFlight[] { flight };
                    //    }
                    //}
                    //string time = dataTimeSearch.ToShortTimeString();
                    #endregion

                    if (dataTimeSearch == DateTime.MinValue)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("\nERROR: Wrong Date Format\n");
                        Console.ForegroundColor = ConsoleColor.Gray;
                        SearchFlightByParameter(1, flights);
                    }
                    var query = from flight in flights
                                where flight.Time.ToShortTimeString() == dataTimeSearch.ToShortTimeString()
                                select flight;
                    foundFlights = query.Contains(null) ? null : query.ToArray();
                    break;
                case 2:
                    //Search by Flight Number
                    Console.WriteLine("Input flight number for search");
                    input = Console.ReadLine();
                    query = from flight in flights
                            where flight.Number.ToString() == input
                            select flight;
                    foundFlights = query.Contains(null) ? null : query.ToArray();
                    break;
                case 3:
                    //Search by Carrier
                    Console.WriteLine("Input Carrier for search");
                    input = Console.ReadLine().Replace(' ', '_');
                    AirlineCompanyEnum carrier;
                    Enum.TryParse(input, out carrier);
                    query = from flight in flights
                            where flight.AirlineCompany == carrier
                            select flight;
                    foundFlights = query.Contains(null) ? null : query.ToArray();
                    break;
                case 4:
                    //Search by Destination
                    Console.WriteLine("Enter the city to search for");
                    input = Console.ReadLine();
                    query = from flight in flights
                            where flight.City == input
                            select flight;
                    foundFlights = query.Contains(null) ? null : query.ToArray();
                    break;
                case 5:
                    //Search by Terminal
                    Console.WriteLine("Enter the terminal name to search for");
                    input = Console.ReadLine();
                    TerminalNameEnum terminalName;
                    Enum.TryParse(input, out terminalName);
                    query = from flight in flights
                            where flight.TerminalName == terminalName
                            select flight;
                    foundFlights = query.Contains(null) ? null : query.ToArray();
                    break;
                case 6:
                    //Search by Terminal Gate
                    Console.WriteLine("Enter the terminal gate number to search for");
                    input = Console.ReadLine();
                    TerminalGateEnum terminalGate;
                    Enum.TryParse(input, out terminalGate);
                    query = from flight in flights
                            where flight.TerminalGate == terminalGate
                            select flight;
                    foundFlights = query.Contains(null) ? null : query.ToArray();
                    break;
                case 7:
                    //Search by Flight Status
                    Console.WriteLine("Enter the flight status to search for");
                    input = Console.ReadLine();
                    if (MenuHelper.TryEnumParse(input, CoreEnum.FlightStatus))
                    {
                        FlightStatusEnum flightStatus = (FlightStatusEnum)Enum.Parse(typeof(FlightStatusEnum), input);
                        query = from flight in flights
                                where flight.FlightStatus == flightStatus
                                select flight;
                        foundFlights = query.Contains(null) ? null : query.ToArray();
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine($"\nError: the value of {input} is not a flight status\n");
                        Console.ResetColor();
                        SearchFlightByParameter(7, MemoryStorage.Flights.ToArray());
                    }
                    break;
                default:
                    Console.WriteLine("\n\t\t\t\t\tYou entered a nonexistent menu item");
                    Console.WriteLine("\n\t\t\t\t\tEnter an item from the list");
                    new PrintManager().PrintMenu();
                    break;
            }

            return foundFlights;
        }
        public IFlight[] SearchFlightByCityAndTime(IFlight[] flights)
        {
            IFlight[] foundFlights = null;
            IFlight[] foundCityFlights = SearchFlightByParameter(4, flights);
            {

                if (foundCityFlights != null)
                {
                    Console.WriteLine("Enter the time in the format yyyy-mm-dd hh:mm");
                    string input = Console.ReadLine();
                    DateTime.TryParse(input, out DateTime dataTimeSearch);
                    //string time = dataTimeSearch.ToShortTimeString();
                    if (dataTimeSearch == DateTime.MinValue)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("\nERROR: Wrong Date Format\n");
                        Console.ForegroundColor = ConsoleColor.Gray;
                    }
                    var query = from flight in flights
                                where flight.Time.Subtract(dataTimeSearch).Hours <= 1
                                select flight;
                    //  in progress
                    foundFlights = query.Contains(null) ? null : query.ToArray();
                }
                else Console.WriteLine("\n\t\t\t\t\t\tCity was not found");
            }
            return foundFlights;
        }
        public void AddFlightInMemoryStorage()
        {

            if (MemoryStorage.Flights == null)
            {
                Console.WriteLine("MemoryStorage.Flights is NULL");
                return;
            }
            int flightType = 0;
            string inputValue = String.Empty;
            Console.WriteLine();
            inputValue = Console.ReadLine();
            int.TryParse(inputValue, out flightType);
            while (flightType != 1 && flightType != 2)
            {
                Console.WriteLine("\n\t\t\t\t\tYou entered a nonexistent menu item");
                Console.WriteLine("\n\t\t\t\t\tEnter an item from the list");
                Console.WriteLine("Please enter Flight Type");
                Console.WriteLine("1. Arrival");
                Console.WriteLine("2. Departure\n");
                AddFlightInMemoryStorage();
            }
            IFlight newFlight = null;
            switch (flightType)
            {
                case 1:
                    newFlight = new Arrival();
                    Console.WriteLine("\nArrival Flight is created\n");
                    break;
                case 2:
                    newFlight = new Departure();
                    Console.WriteLine("\nDeparture Flight is created\n");
                    break;
            }
            newFlight.Number = MenuHelper.GetFlightNumber();
            Console.WriteLine($"\nNumber for flight is {newFlight.Number}\n");
            newFlight.TerminalGate = MenuHelper.GetTerminalGate();
            Console.WriteLine($"\nThe gate number for the flight is {newFlight.TerminalGate.ToString()}\n");
            newFlight.TerminalName = MenuHelper.GetTerminalName();
            Console.WriteLine($"\nThe terminal name for the flight is {newFlight.TerminalName.ToString()}\n");
            newFlight.AirlineCompany = MenuHelper.GetCarrier();
            Console.WriteLine($"\nThe carrier name for the flight is {newFlight.AirlineCompany.ToString().Replace('_', ' ')}");
            newFlight.FlightStatus = MenuHelper.GetFlightStatus();
            Console.WriteLine($"\nThe status for the flight is {newFlight.FlightStatus.ToString().Replace('_', ' ')}");
            newFlight.City = MenuHelper.GetCityName();
            Console.WriteLine($"\nThe destination for the flight is {newFlight.City}");
            newFlight.Time = MenuHelper.GetFlightTime();
            Console.WriteLine($"\nThe time for the flight is {newFlight.Time.ToShortTimeString()}\n");
            if (newFlight is Arrival)
            {
                MemoryStorage.Arrivals.Add((Arrival)newFlight);
            }
            else
            {
                MemoryStorage.Departures.Add((Departure)newFlight);
            }

        }
        public void EditFlightInMemoryStorage(int flightNumber, FlightFieldsEnum changedParameter)
        {
            if (MemoryStorage.Flights == null || MemoryStorage.Flights.Count() == 0)
            {
                Console.WriteLine("\n\t\t\t\t\tTable of flights empty");
                return;
            }
            int flightIndex = MemoryStorage.Flights.FindIndex(x => x.Number.Equals(flightNumber));
            if (MemoryStorage.Flights.ElementAt(flightIndex) is Arrival)
            {
                switch (changedParameter)
                {
                    case FlightFieldsEnum.Time:
                        MemoryStorage.Arrivals[flightIndex].Time = MenuHelper.GetFlightTime();
                        break;
                    case FlightFieldsEnum.Number:
                        MemoryStorage.Arrivals[flightIndex].Number = MenuHelper.GetFlightNumber();
                        break;
                    case FlightFieldsEnum.AirlineCompany:
                        MemoryStorage.Arrivals[flightIndex].AirlineCompany = MenuHelper.GetCarrier();
                        break;
                    case FlightFieldsEnum.City:
                        MemoryStorage.Arrivals[flightIndex].City = MenuHelper.GetCityName();
                        break;
                    case FlightFieldsEnum.TerminalName:
                        MemoryStorage.Arrivals[flightIndex].TerminalName = MenuHelper.GetTerminalName();
                        break;
                    case FlightFieldsEnum.TerminalGate:
                        MemoryStorage.Arrivals[flightIndex].TerminalGate = MenuHelper.GetTerminalGate();
                        break;
                    case FlightFieldsEnum.FlightStatus:
                        MemoryStorage.Arrivals[flightIndex].FlightStatus = MenuHelper.GetFlightStatus();
                        break;
                    default:
                        Console.WriteLine("ERROR: Wrong changed parameter");
                        break;
                }
            }
            else
            {
                switch (changedParameter)
                {
                    case FlightFieldsEnum.Time:
                        MemoryStorage.Flights[flightIndex].Time = MenuHelper.GetFlightTime();
                        break;
                    case FlightFieldsEnum.Number:
                        MemoryStorage.Flights[flightIndex].Number = MenuHelper.GetFlightNumber();
                        break;
                    case FlightFieldsEnum.AirlineCompany:
                        MemoryStorage.Flights[flightIndex].AirlineCompany = MenuHelper.GetCarrier();
                        break;
                    case FlightFieldsEnum.City:
                        MemoryStorage.Flights[flightIndex].City = MenuHelper.GetCityName();
                        break;
                    case FlightFieldsEnum.TerminalName:
                        MemoryStorage.Flights[flightIndex].TerminalName = MenuHelper.GetTerminalName();
                        break;
                    case FlightFieldsEnum.TerminalGate:
                        MemoryStorage.Flights[flightIndex].TerminalGate = MenuHelper.GetTerminalGate();
                        break;
                    case FlightFieldsEnum.FlightStatus:
                        MemoryStorage.Flights[flightIndex].FlightStatus = MenuHelper.GetFlightStatus();
                        break;
                    default:
                        Console.WriteLine("ERROR: Wrong changed parameter");
                        break;
                }
            }
        }
        public bool SearchFlightByNumberInMemoryStorage(int flightNumber)
        {
            return MemoryStorage.Flights.Exists(x => x.Number.Equals(flightNumber));
        }
        public void DeleteFlightByNumberInMemoryStorage(int flightNumber)
        {
            int index = MemoryStorage.Flights.FindIndex(x => x.Number.Equals(flightNumber));
            IFlight flight = MemoryStorage.Flights.ElementAt(index);
            if (flight is Arrival)
            {
                MemoryStorage.Arrivals.Remove((Arrival)flight);
            }
            else MemoryStorage.Departures.Remove((Departure)flight);
        }
    }
}
