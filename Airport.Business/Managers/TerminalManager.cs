﻿using System.Linq;
using Airport.Core.Interfaces.Managers;
using Airport.Core.Interfaces.Repositories;
using Airport.Core.Interfaces.UnitOfWork;
using Airport.Core.Models;

namespace Airport.Business.Managers
{
    public class TerminalManager : ITerminalManager
	{
        private readonly ITerminalRepository _terminalRepository;
        private readonly IUnitOfWork _unitOfWork;

        public TerminalManager(IUnitOfWork unitOfWork,
							  ITerminalRepository terminalRepository)
        {
	        _terminalRepository = terminalRepository;
            _unitOfWork = unitOfWork;
        }

        public Terminal[] GetAllTerminals()
        {
            return _terminalRepository.SelectAll().ToArray();
        }

        public Terminal GetTerminalById(int terminalId)
        {
            return _terminalRepository.Find(terminalId);
        }

        public void AddTerminal(string terminalName)
        {
            var terminal = new Terminal
            {
                TerminalName = terminalName,
            };

            _terminalRepository.Insert(terminal);
            _unitOfWork.Save();
        }

        public void EditTerminal(int terminalId, string terminalName)
        {
            var record = _terminalRepository.Get(terminalId);
            if (record != null)
            {
                record.TerminalName = terminalName;
                _unitOfWork.Save();
            }
        }
        public void DeleteTerminal(int terminalId)
        {
            var record = _terminalRepository.Find(terminalId);
            if (record != null)
            {
                _terminalRepository.Delete(record);
                _unitOfWork.Save();
            }
        }
    }
}
