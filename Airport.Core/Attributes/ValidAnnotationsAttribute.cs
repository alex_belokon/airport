﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Airport.Core.Attributes
{
    public class ValidAnnotationsAttribute : ValidationAttribute
    {
        private readonly string[] _mytitles;

        public ValidAnnotationsAttribute(string[] myTitles)
        {
            _mytitles = myTitles;
        }

        public override bool IsValid(object value)
        {
            var result = false;
            if (value != null)
            {
                var enteredVal = value.ToString();
                result = _mytitles.FirstOrDefault(x => string.Equals(x, enteredVal, StringComparison.InvariantCultureIgnoreCase)) != null;
            }
            return result;
        }
    }
}
