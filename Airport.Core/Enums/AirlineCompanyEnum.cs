﻿namespace Airport.Core.Enums
{
    public enum AirlineCompanyEnum
    {
        Belavia,
        Air_Baltic,
        Turkish_Airlines,
        Egypt_Air,
        Air_France,
        Dniproavia,
        Wind_Rose,
        KLM,
        Austrian_Airlines,
        Lufthansa,
        Azur_Air_Ukraine,
        Atlasjet_Ukraine,
        Azerbaijan_Airlines
    }
}
