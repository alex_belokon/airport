﻿namespace Airport.Core.Enums
{
    public enum FlightStatusEnum
    {
        CheckIn,
        Gate_Closed,
        Arrived,
        Departed,
        Unknown,
        Canceled,
        Excpected,
        Delayed,
    }
}
