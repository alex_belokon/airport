﻿namespace Airport.Core.Enums
{
    public enum FlightTypeEnum
    {
        Arrived,
        Departed
    }
}
