﻿using System.Collections.Generic;
using Airport.Core.BusinnessModels;
using Airport.Core.Enums;

namespace Airport.Core
{
    public interface IFlightManager
    {
        List<Arrival> GetArrivals();
        List<Departure> GetDepartures();
        IFlight[] SearchFlightByParameter(int searchParameter, IFlight[] flights);
        IFlight[] SearchFlightByCityAndTime(IFlight[] flights);
        void AddFlightInMemoryStorage();
        void EditFlightInMemoryStorage(int flightNumber, FlightFieldsEnum changedParameter);
        bool SearchFlightByNumberInMemoryStorage(int flightNumber);
        void DeleteFlightByNumberInMemoryStorage(int flightNumber);
    }
}
