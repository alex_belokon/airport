﻿using Airport.Core.Models;

namespace Airport.Core.Interfaces.Managers
{
    public interface ICityManager
    {
        City[] GetAllCities();

        City GetCityById(int cityId);

        void AddCity(string cityName);

        void EditCity(int cityId, string cityName);

        void DeleteCity(int cityId);
    }
}
