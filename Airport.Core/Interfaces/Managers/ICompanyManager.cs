﻿using Airport.Core.Models;

namespace Airport.Core.Interfaces.Managers
{
    public interface ICompanyManager
    {
        Company[] GetAllCompanies();

        Company GetCompanyById(int companyId);

        void AddCompany(string companyName, int cityId);

        void EditCompany(int companyId, string companyName, int companyCityId);

        void DeleteCompany(int companyId);
    }
}
