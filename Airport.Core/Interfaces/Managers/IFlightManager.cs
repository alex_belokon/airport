﻿using System;
using Airport.Core.Models;

namespace Airport.Core.Interfaces.Managers
{
    public interface IFlightManager
    {
	    Flight[] GetAllFlights();

	    Flight GetFlightById(int flightId);

		void AddFlight(string flightNumber, DateTime flightDateTime, TimeSpan flightDuration, int flightStatusId,

			           int cityFromId, int cityToId, int companyId, int terminalId);

	    void EditFlight(int flightId, string flightNumber, DateTime flightDateTime, TimeSpan flightDuration, int flightStatusId,

						int cityFromId, int cityToId, int companyId, int terminalId);

	    void DeleteFlight(int flightId);
    }
}
