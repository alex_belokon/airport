﻿using Airport.Core.Models;

namespace Airport.Core.Interfaces.Managers
{
    public interface ITerminalManager
    {
        Terminal[] GetAllTerminals();

        Terminal GetTerminalById(int terminalId);

        void AddTerminal(string terminalName);

        void EditTerminal(int terminalId, string terminalName);

        void DeleteTerminal(int terminalId);
    }
}
