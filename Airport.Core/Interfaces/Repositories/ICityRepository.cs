﻿using Airport.Core.Models;

namespace Airport.Core.Interfaces.Repositories
{
    public interface ICityRepository : IRepositoryBase<City>
    {
        City Get(int id);
    }
}
