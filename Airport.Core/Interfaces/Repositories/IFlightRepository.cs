﻿using Airport.Core.Models;

namespace Airport.Core.Interfaces.Repositories
{
    public interface IFlightRepository : IRepositoryBase<Flight>
    {
	    Flight Get(int id);
    }
}
