﻿using Airport.Core.Models;

namespace Airport.Core.Interfaces.Repositories
{
    public interface ITerminalRepository : IRepositoryBase<Terminal>
    {
        Terminal Get(int id);
    }
}
