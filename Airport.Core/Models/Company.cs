﻿using System.Collections.Generic;

namespace Airport.Core.Models
{
    public class Company
    {
        public int CompanyId  { get; set; }

        public string CompanyName { get; set; }

        public int CityId { get; set; }

	    public virtual City City { get; set; }

	    public virtual ICollection<Flight> Flights { get; set; }
	}
}
