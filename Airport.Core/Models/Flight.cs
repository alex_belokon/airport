﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Airport.Core.Enums;

namespace Airport.Core.Models
{
	public class Flight
	{
		public int FlightId { get; set; }
		public string FlightNumber { get; set; }
		public DateTime FlightDateTime { get; set; }
		public TimeSpan FlightDuration { get; set; }
		public int FlightStatusId { get; set; }
		public int CityFromId { get; set; }
		public int CityToId { get; set; }
		public int CompanyId { get; set; }
		public int TerminalId { get; set; }

		public virtual Terminal Terminal { get; set; }
		public virtual Company Company { get; set; }
		public virtual City CityFrom { get; set; }
		public virtual City CityTo { get; set; }

	}
}
