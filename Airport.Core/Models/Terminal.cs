﻿using System.Collections.Generic;

namespace Airport.Core.Models
{
    public class Terminal
    {
        public int TerminalId  { get; set; }

        public string TerminalName { get; set; }

		public virtual ICollection<Flight> Flights { get; set; }
	}
}
