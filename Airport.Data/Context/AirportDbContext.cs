﻿using System.Data.Entity;
using Airport.Core;
using Airport.Core.Interfaces;
using Airport.Core.Models;
using Airport.Data.Mapping;

namespace Airport.Data.Context
{
    public class AirportDbContext : DbContext, IAirportDbContext
    {
        public AirportDbContext() : base("name=AirportEntities")
        {
            //this.Database.Log = txt => System.Diagnostics.Debug.WriteLine(txt);

            //The Entity Framework provider type 'System.Data.Entity.SqlServer.SqlProviderServices, EntityFramework.SqlServer'
            //for the 'System.Data.SqlClient' ADO.NET provider could not be loaded.
            //Make sure the provider assembly is available to the running application.
            //See http://go.microsoft.com/fwlink/?LinkId=260882 for more information.

            var ensureDLLIsCopied = System.Data.Entity.SqlServer.SqlProviderServices.Instance;
        }

        public AirportDbContext(string connectionStringName) : base("name=" + connectionStringName)
        {
            var ensureDLLIsCopied = System.Data.Entity.SqlServer.SqlProviderServices.Instance;
        }

        public virtual DbSet<Company> Companies { get; set; }
	    public virtual DbSet<City> Cities { get; set; }
	    public virtual DbSet<Terminal> Terminal { get; set; }
	    public virtual DbSet<Flight> Flights { get; set; }


		protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // don't create database at startup
            Database.SetInitializer<AirportDbContext>(null);

            modelBuilder.Configurations.Add(new CompanyMap());
	        modelBuilder.Configurations.Add(new CityMap());
	        modelBuilder.Configurations.Add(new TerminalMap());
	        modelBuilder.Configurations.Add(new FlightMap());
		}
    }
}
