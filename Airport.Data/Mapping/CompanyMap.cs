﻿using System.Data.Entity.ModelConfiguration;
using Airport.Core.Models;

namespace Airport.Data.Mapping
{
    public class CompanyMap : EntityTypeConfiguration<Company>
    {
        public CompanyMap()
        {
            // Primary Key
            this.HasKey(t => t.CompanyId);

            // Properties


            // Table & Column Mappings
            this.ToTable("Company");
            //this.Property(x => x.CompanyName).HasColumnName("c1");

            // Navigation properties
	        this.HasRequired(t => t.City);
	        this.HasMany(t => t.Flights);
		}
    }
}
