﻿using System.Data.Entity.ModelConfiguration;
using Airport.Core.Models;

namespace Airport.Data.Mapping
{
	public class FlightMap : EntityTypeConfiguration<Flight>
	{
		public FlightMap()
		{
			this.HasKey(t => t.FlightId);

		    //Ignore(t => t.FlightDuration);

			this.ToTable("Flight");

			this.HasRequired(t => t.Company);

			this.HasRequired(t => t.Terminal);

			this.HasRequired(t => t.CityFrom).WithMany().HasForeignKey(t => t.CityFromId);

			this.HasRequired(t => t.CityTo).WithMany().HasForeignKey(t => t.CityToId);

		}
	}
}
