﻿using System.Data.Entity.ModelConfiguration;
using Airport.Core.Models;

namespace Airport.Data.Mapping
{
    public class TerminalMap : EntityTypeConfiguration<Terminal>
    {
        public TerminalMap()
        {
            // Primary Key
            this.HasKey(t => t.TerminalId);

            // Properties


            // Table & Column Mappings
            this.ToTable("Terminal");

			// Navigation properties
			this.HasMany(t => t.Flights);
		}
    }
}
