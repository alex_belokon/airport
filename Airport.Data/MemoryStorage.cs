﻿using System.Collections.Generic;
using Airport.Core;
using Airport.Core.BusinnessModels;
using Airport.Core.Interfaces;

namespace Airport.Data
{
    public static class MemoryStorage
    {

        public static List<Arrival> Arrivals { get; set; }
        public static List<Departure> Departures { get; set; }
        public static List<IFlight> Flights
        {
            get
            {
                List<IFlight> result = new List<IFlight>();
                if (Arrivals == null && Departures == null)
                {
                    return result;
                }
                else
                {
                    result.AddRange(MemoryStorage.Arrivals);
                    result.AddRange(MemoryStorage.Departures);
                    return result;
                }
            }
        }
    }
}
