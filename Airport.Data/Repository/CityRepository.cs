﻿using System.Linq;
using Airport.Core.Interfaces.Repositories;
using Airport.Core.Interfaces.UnitOfWork;
using Airport.Core.Models;

namespace Airport.Data.Repository
{
    public class CityRepository: RepositoryBase<City>, ICityRepository
    {
        public CityRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public City Get(int id)
        {
            return this.Select(x => x.CityId == id).FirstOrDefault();
        }
    }
}
