﻿using System.Linq;
using Airport.Core.Interfaces.Repositories;
using Airport.Core.Interfaces.UnitOfWork;
using Airport.Core.Models;

namespace Airport.Data.Repository
{
    public class FlightRepository: RepositoryBase<Flight>, IFlightRepository
	{
        public FlightRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public Flight Get(int id)
        {
            return this.Select(x => x.FlightId == id).FirstOrDefault();
        }
    }
}
