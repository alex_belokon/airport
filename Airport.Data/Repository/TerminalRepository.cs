﻿using System.Linq;
using Airport.Core.Interfaces.Repositories;
using Airport.Core.Interfaces.UnitOfWork;
using Airport.Core.Models;

namespace Airport.Data.Repository
{
    public class TerminalRepository: RepositoryBase<Terminal>, ITerminalRepository
    {
        public TerminalRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public Terminal Get(int id)
        {
            return this.Select(x => x.TerminalId == id).FirstOrDefault();
        }
    }
}
