﻿CREATE TABLE [dbo].[City] (
    [CityId]   INT          IDENTITY (1, 1) NOT NULL,
    [CityName] VARCHAR (50) NULL,
    CONSTRAINT [PK_City_CityId] PRIMARY KEY CLUSTERED ([CityId] ASC)
);

