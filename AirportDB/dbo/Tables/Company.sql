﻿CREATE TABLE [dbo].[Company] (
    [CompanyId]   INT          IDENTITY (1, 1) NOT NULL,
    [CompanyName] VARCHAR (50) NULL,
    [CityId]      INT          NOT NULL,
    CONSTRAINT [PK_Company_CompanyId] PRIMARY KEY CLUSTERED ([CompanyId] ASC),
    CONSTRAINT [FK_CityId] FOREIGN KEY ([CityId]) REFERENCES [dbo].[City] ([CityId]) ON DELETE CASCADE ON UPDATE CASCADE
);



