﻿CREATE TABLE [dbo].[Flight] (
    [FlightId]       INT          IDENTITY (1, 1) NOT NULL,
    [FlightNumber]   VARCHAR (50) NULL,
    [FlightDateTime] DATETIME     NULL,
    [FlightDuration] TIME (7)     NULL,
    [FlightStatusId] INT          NULL,
    [CityFromId]     INT          NULL,
    [CityToId]       INT          NULL,
    [CompanyId]      INT          NULL,
    [TerminalId]     INT          NULL,
    CONSTRAINT [PK_Flight_FlightId] PRIMARY KEY CLUSTERED ([FlightId] ASC),
    CONSTRAINT [FK_Flight_CityFromId] FOREIGN KEY ([CityFromId]) REFERENCES [dbo].[City] ([CityId]),
    CONSTRAINT [FK_Flight_CityToId] FOREIGN KEY ([CityToId]) REFERENCES [dbo].[City] ([CityId]),
    CONSTRAINT [FK_Flight_CompanyId] FOREIGN KEY ([CompanyId]) REFERENCES [dbo].[Company] ([CompanyId]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_Flight_TerminalId] FOREIGN KEY ([TerminalId]) REFERENCES [dbo].[Terminal] ([TerminalId]) ON DELETE CASCADE ON UPDATE CASCADE
);



