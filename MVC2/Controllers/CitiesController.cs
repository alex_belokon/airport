﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Airport.Core.Interfaces.Managers;
using MVC2.Models.Companies;

namespace MVC2.Controllers
{
	public class CitiesController : Controller
    {
	    public CitiesController()
	    {

	    }
	    private readonly ICityManager _cityManager;
	    //private readonly List<CityModel> _cities;

	    public CitiesController(ICityManager cityManager)
	    {
			_cityManager = cityManager;
		    
		}
		// GET: Cities
		public ActionResult Index()
		{
			ViewBag.RoleAdmin = User.IsInRole("Admin");
	        var cities = _cityManager.GetAllCities();
	        return View(cities);
        }
	    [Authorize(Roles = "Admin")]
		public ActionResult AddNewCity()
		{
			var model = new CityModel();

			return View("CityDetails", model);
		}

	    [ValidateAntiForgeryToken]
	    [HttpPost]
	    [Authorize(Roles = "Admin")]
		public ActionResult AddNewCity(CityModel city)
	    {
		    if (!string.IsNullOrEmpty(city.CityName))
		    {
			    var record = _cityManager.GetAllCities()
				    .FirstOrDefault(x => string.Equals(x.CityName, city.CityName, StringComparison.InvariantCultureIgnoreCase));
			    if (record != null) ModelState.AddModelError(nameof(city.CityName), $"We already have city '{city.CityName}' in DB");
		    }

		    if (ModelState.IsValid)
		    {
			    _cityManager.AddCity(city.CityName);
			    return RedirectToAction("Index");
		    }

		    return View("CityDetails", city);
	    }
	    [Authorize(Roles = "Admin")]
		public ActionResult EditCity(int CityId)
	    {
		    var model = GetModel(CityId);
		    return View("CityDetails", model);
	    }

		[ValidateAntiForgeryToken]
		[HttpPost]
		[Authorize(Roles = "Admin")]
		public ActionResult EditCity(CityModel city)
		{
			if (!string.IsNullOrEmpty(city.CityName))
			{
				var oldCity = _cityManager.GetCityById(city.CityId);
				if (!oldCity.CityName.Equals(city.CityName, StringComparison.InvariantCultureIgnoreCase))
				{
					var record = _cityManager.GetAllCities()
						.FirstOrDefault(x => string.Equals(x.CityName, city.CityName, StringComparison.InvariantCultureIgnoreCase));
					if (record != null)
						ModelState.AddModelError(nameof(city.CityName), $"We already have city '{city.CityName}' in DB");
				}
			}
			if (ModelState.IsValid)
			{
				_cityManager.EditCity(city.CityId, city.CityName);
				return RedirectToAction("Index");
			}

			return View("CityDetails", city);
		}

		private CityModel GetModel(int cityId)
	    {
		    var city = _cityManager.GetCityById(cityId);
		    var model = new CityModel
		    {
			    CityId = city.CityId,
			    CityName = city.CityName,
			   
		    };

		    return model;
	    }
        //[Authorize(Roles = "Admin")]
        //public ActionResult DeleteCity(int cityId)
        //{
        //    try
        //    {

        //        var model = GetModel(cityId);
        //        return View(model);
        //    }

        //    catch (Exception)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }

        //}

       
 
        [Authorize(Roles = "Admin")]
        public ActionResult DeleteCity(int cityId)
        {

                    _cityManager.DeleteCity(cityId);
                    return RedirectToAction("Index");
       
        }
    }
}