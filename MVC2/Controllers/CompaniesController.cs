﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Web.Mvc;
using Airport.Core.Interfaces.Managers;
using MVC2.Models.Companies;

namespace MVC2.Controllers
{
	public class CompaniesController : Controller
	{

		public CompaniesController()
		{

		}
		private readonly ICompanyManager _companyManager;
		//private readonly ICityManager _cityManager;
		private readonly List<CityModel> _cities;

		public CompaniesController(ICompanyManager companyManager, ICityManager cityManager)
		{
			_companyManager = companyManager;
			//_cityManager = cityManager;

			_cities = cityManager.GetAllCities().Select(x => new CityModel()
			{
				CityId = x.CityId,
				CityName = x.CityName
			}).ToList();

			//_cities = new List<CityModel>(new[]     // TODO: get cities from _cityRepository
			//{
			//    new CityModel
			//    {
			//        CityId = 1,
			//        CityName = "Kiev"
			//    },
			//    new CityModel
			//    {
			//        CityId = 2,
			//        CityName = "Odessa"
			//    }
			//});
		}

		// GET: Companies
		//[OutputCache(Duration = 300)] // 5 minutes
		public ActionResult Index()
		{
			ViewBag.RoleAdmin = User.IsInRole("Admin");
			var companies = _companyManager.GetAllCompanies();
			return View(companies);
		}

		[Authorize(Roles = "Admin")]
		public ActionResult AddNewCompany()
		{
			var model = new CompanyModel
			{
				Cities = _cities
			};

			return View("CompanyDetails", model);
		}

		[ValidateAntiForgeryToken]
		[HttpPost]
		[Authorize(Roles = "Admin")]
		public ActionResult AddNewCompany(CompanyModel company)
		{
			// unique CompanyName
			if (!string.IsNullOrEmpty(company.CompanyName))
			{
				var record = _companyManager.GetAllCompanies()
											.FirstOrDefault(x => string.Equals(x.CompanyName, company.CompanyName, StringComparison.InvariantCultureIgnoreCase));
				company.Cities = _cities;
				if (record != null) ModelState.AddModelError(nameof(company.CompanyName), $"We already have company '{company.CompanyName}' in DB");
			}

			if (ModelState.IsValid)
			{
				_companyManager.AddCompany(company.CompanyName, company.SelectedCityId);
				return RedirectToAction("Index");
			}

			return View("CompanyDetails", company);
		}

		[Authorize(Roles = "Admin")]
		public ActionResult EditCompany(int companyId)
		{
			var model = GetModel(companyId);
			return View("CompanyDetails", model);
		}

		[ValidateAntiForgeryToken]
		[HttpPost]
		[Authorize(Roles = "Admin")]
		public ActionResult EditCompany(CompanyModel company)
		{
			if (!string.IsNullOrEmpty(company.CompanyName))
			{
				var oldCompany = _companyManager.GetCompanyById(company.CompanyId);
				if (!oldCompany.CompanyName.Equals(company.CompanyName, StringComparison.InvariantCultureIgnoreCase))
				{
					var record = _companyManager.GetAllCompanies()
						.FirstOrDefault(x => string.Equals(x.CompanyName, company.CompanyName, StringComparison.InvariantCultureIgnoreCase));
					company.Cities = _cities;
					if (record != null) ModelState.AddModelError(nameof(company.CompanyName), $"We already have company '{company.CompanyName}' in DB");
				}
			}
			if (ModelState.IsValid)
			{
				_companyManager.EditCompany(company.CompanyId, company.CompanyName, company.SelectedCityId);
				return RedirectToAction("Index");
			}

			return View("CompanyDetails", company);
		}

		public ActionResult CompanyPreview(int companyid)
		{
			var model = GetModel(companyid);
			return PartialView(model);
		}

		private CompanyModel GetModel(int companyId)
		{
			var company = _companyManager.GetCompanyById(companyId);
			var model = new CompanyModel
			{
				CompanyId = company.CompanyId,
				CompanyName = company.CompanyName,
				Cities = _cities,
				SelectedCityId = company.CityId,
				SelectedCityName = _cities.FirstOrDefault(x => x.CityId == company.CityId)?.CityName ?? ""
			};

			return model;
		}
        [Authorize(Roles = "Admin")]
        public ActionResult DeleteCompany(int companyId)
        {
            try
            {

                var model = GetModel(companyId);
                return View(model);
            }

            catch (Exception)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        [Authorize(Roles = "Admin")]
        public ActionResult DeleteCompany(CompanyModel company)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _companyManager.DeleteCompany(company.CompanyId);
                    return RedirectToAction("Index");
                }
                else
                {
                    return HttpNotFound();
                }

            }

            catch (Exception)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }
    }
}