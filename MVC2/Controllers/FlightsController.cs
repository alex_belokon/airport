﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Airport.Core.Enums;
using Airport.Core.Interfaces.Managers;
using Airport.Core.Interfaces.Repositories;
using Airport.Core.Models;
using MVC2.Models.Flights;

namespace MVC2.Controllers
{
	public class FlightsController : Controller
	{
		private readonly IFlightManager _flightManager;
		private readonly ICompanyRepository _companyRepository;
		private readonly ICityRepository _cityRepository;
		private readonly ITerminalRepository _terminalRepository;
		private readonly List<City> _cities;
		private readonly List<Company> _companies;
		private readonly List<Terminal> _terminals;

		public FlightsController() : base()
		{


		}
		public FlightsController(IFlightManager flightManager,
								 ICompanyRepository companyRepository,
								 ICityRepository cityRepository,
								 ITerminalRepository terminalRepository)
		{
			_flightManager = flightManager;
			_companyRepository = companyRepository;
			_cityRepository = cityRepository;
			_terminalRepository = terminalRepository;
			_companies = companyRepository.SelectAll().OrderBy(x => x.CompanyName).ToList();
			_cities = cityRepository.SelectAll().OrderBy(x => x.CityName).ToList();
			_terminals = terminalRepository.SelectAll().OrderBy(x => x.TerminalName).ToList();
		}
		// GET: Flights
		public ActionResult Index()
		{
			ViewBag.RoleAdmin = User.IsInRole("Admin");
			var flights = _flightManager.GetAllFlights();
			return View(flights);
		}
		[Authorize(Roles = "Admin")]
		public ActionResult AddNewFlight()
		{
			var model = new FlightModel
			{
				Cities = _cities,
				Companies = _companies,
				Terminals = _terminals
			};

			return View("FlightDetails", model);
		}

		[ValidateAntiForgeryToken]
		[HttpPost]
		[Authorize(Roles = "Admin")]
		public ActionResult AddNewFlight(FlightModel flight)
		{
			if (!string.IsNullOrEmpty(flight.FlightNumber))
			{
				var record = _flightManager.GetAllFlights()
					.FirstOrDefault(x => string.Equals(x.FlightNumber, flight.FlightNumber, StringComparison.InvariantCultureIgnoreCase));
				if (record != null) ModelState.AddModelError(nameof(flight.FlightNumber), $"We already have flight '{flight.FlightNumber}' in DB");
                if (flight.SelectedCityToId == flight.SelectedCityFromId) ModelState.AddModelError(nameof(flight.SelectedCityToId), $"You cannot select the same city for departure and arrival");
            }

			if (ModelState.IsValid)
			{
				_flightManager.AddFlight(flight.FlightNumber, flight.FlightDateTime, flight.FlightDuration, (int)flight.FlightStatusId,
										 flight.SelectedCityFromId, flight.SelectedCityToId, flight.SelectedCompanyId, flight.SelectedTerminalId);
				return RedirectToAction("Index");
			}
			flight.Terminals = _terminals;
			flight.Cities = _cities;
			flight.Companies = _companies;
			return View("FlightDetails", flight);
		}

		[Authorize(Roles = "Admin")]
		public ActionResult EditFlight(int flightId)
		{
			var model = GetModel(flightId);
			return View("FlightDetails", model);
		}

		[ValidateAntiForgeryToken]
		[HttpPost]
		[Authorize(Roles = "Admin")]
		public ActionResult EditFlight(FlightModel flight)
		{
           
            if (flight.SelectedCityToId == flight.SelectedCityFromId) ModelState.AddModelError(nameof(flight.SelectedCityToId), $"You cannot select the same city for departure and arrival");
            if (ModelState.IsValid)
			{
				_flightManager.EditFlight(flight.FlightId, flight.FlightNumber, flight.FlightDateTime, flight.FlightDuration, (int)flight.FlightStatusId,
					flight.SelectedCityFromId, flight.SelectedCityToId, flight.SelectedCompanyId, flight.SelectedTerminalId);
				return RedirectToAction("Index");
			}
			flight.Terminals = _terminals;
			flight.Cities = _cities;
			flight.Companies = _companies;
			return View("FlightDetails", flight);
		}
		private FlightModel GetModel(int flightId)
		{
			var flight = _flightManager.GetFlightById(flightId);
			var model = new FlightModel
			{
				FlightId = flight.FlightId,
				FlightNumber = flight.FlightNumber,
				FlightDateTime = flight.FlightDateTime,
				FlightDuration = flight.FlightDuration,
				Cities = _cities,
				SelectedCityFromId = flight.CityFromId,
				SelectedCityToId = flight.CityToId,
				Companies = _companies,
				SelectedCompanyId = flight.CompanyId,
				Terminals = _terminals,
				SelectedTerminalId = flight.TerminalId,
				FlightStatusId = (FlightStatusEnum)flight.FlightStatusId
			};

			return model;
		}
		[Authorize(Roles = "Admin")]
		public ActionResult DeleteFlight(int flightId)
		{
			try
			{

				var model = GetModel(flightId);
				return View(model);
			}

			catch (Exception)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}

		}

		[ValidateAntiForgeryToken]
		[HttpPost]
		[Authorize(Roles = "Admin")]
		public ActionResult DeleteFlight(FlightModel flight)
		{
			try
			{
				if (ModelState.IsValid)
				{
					_flightManager.DeleteFlight(flight.FlightId);
					return RedirectToAction("Index");
				}
				else
				{
					return HttpNotFound();
				}
				//flight.Terminals = _terminals;
				//flight.Cities = _cities;
				//flight.Companies = _companies;
				//return View("FlightDetails", flight);
			}

			catch (Exception)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
		}
	}
}