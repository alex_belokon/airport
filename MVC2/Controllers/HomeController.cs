﻿using System.Web.Mvc;
using System.Web.Http;
using Airport.Core.Interfaces.Managers;
using Microsoft.AspNet.Identity;
using MVC2.Models.Home;

namespace MVC2.Controllers
{
	public class HomeController : Controller
    {

	    public HomeController()
	    {
		    
	    }
        private readonly ICompanyManager _companyManager;

        public HomeController(ICompanyManager companyManager)
        {
            _companyManager = companyManager;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult CustomMethod([FromUri(Name = "")] CustomRequest myRequest)  //string name, string edu, bool married
        {
            ViewBag.Message = "Your contact page.";

            ViewBag.Name = myRequest.Name ?? "";
            ViewBag.Edu = myRequest.Edu ?? "";
            ViewBag.Married = myRequest.Married.ToString();

            return View();
        }

        public string GetCompanyName(string id)
        {
            var companyId = 0;
            int.TryParse(id, out companyId);

            var cityName = _companyManager.GetCompanyById(companyId)?.CompanyName;
            return cityName;
        }

        [System.Web.Mvc.HttpPost]
        public string AddToFavorites(string id)
        {
            var result = string.Empty;

            if (Request.IsAuthenticated)
            {
                var userid = User.Identity.GetUserId();
                var productId = 0;

                int.TryParse(id, out productId);
                result = $"Product with Id: {productId} was added to favorites";

                // TODO: use manager to add to DB productId with UserId
            }
            else
            {
                result = "Pls login to do that !";
            }

            return result;

        }
    }
}