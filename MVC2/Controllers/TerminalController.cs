﻿using System;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Airport.Core.Interfaces.Managers;
using MVC2.Models.Terminals;

namespace MVC2.Controllers
{
	public class TerminalController : Controller
    {
	    public TerminalController()
	    {
		    
	    }
	    private readonly ITerminalManager _terminalManager;

	    public TerminalController(ITerminalManager terminalManager)
	    {
			_terminalManager = terminalManager;
		    
		}
		// GET: terminals
		public ActionResult Index()
        {
	        ViewBag.RoleAdmin = User.IsInRole("Admin");
			var terminals = _terminalManager.GetAllTerminals();
	        return View(terminals);
        }
	    [Authorize(Roles = "Admin")]
		public ActionResult AddNewTerminal()
		{
			var model = new TerminalModel();

			return View("TerminalDetails", model);
		}

		[ValidateAntiForgeryToken]
		[HttpPost]
		[Authorize(Roles = "Admin")]
		public ActionResult AddNewTerminal(TerminalModel terminal)
		{
			if (!string.IsNullOrEmpty(terminal.TerminalName))
			{
				var record = _terminalManager.GetAllTerminals()
					.FirstOrDefault(x => string.Equals(x.TerminalName, terminal.TerminalName, StringComparison.InvariantCultureIgnoreCase));
				if (record != null) ModelState.AddModelError(nameof(terminal.TerminalName), $"We already have terminal '{terminal.TerminalName}' in DB");
			}

			if (ModelState.IsValid)
			{
				_terminalManager.AddTerminal(terminal.TerminalName);
				return RedirectToAction("Index");
			}

			return View("TerminalDetails", terminal);
		}
	    [Authorize(Roles = "Admin")]
		public ActionResult EditTerminal(int TerminalId)
		{
			var model = GetModel(TerminalId);
			return View("TerminalDetails", model);
		}

		[ValidateAntiForgeryToken]
		[HttpPost]
		[Authorize(Roles = "Admin")]
		public ActionResult EditTerminal(TerminalModel terminal)
		{
			if (!string.IsNullOrEmpty(terminal.TerminalName))
			{
				var oldTerminal = _terminalManager.GetTerminalById(terminal.TerminalId);
				if (!oldTerminal.TerminalName.Equals(terminal.TerminalName, StringComparison.InvariantCultureIgnoreCase))
				{
					var record = _terminalManager.GetAllTerminals()
						.FirstOrDefault(x =>
							string.Equals(x.TerminalName, terminal.TerminalName, StringComparison.InvariantCultureIgnoreCase));
					if (record != null)
						ModelState.AddModelError(nameof(terminal.TerminalName),
							$"We already have terminal '{terminal.TerminalName}' in DB");
				}
			}
			if (ModelState.IsValid)
			{
				_terminalManager.EditTerminal(terminal.TerminalId, terminal.TerminalName);
				return RedirectToAction("Index");
			}

			return View("TerminalDetails", terminal);
		}

		private TerminalModel GetModel(int terminalId)
		{
			var terminal = _terminalManager.GetTerminalById(terminalId);
			var model = new TerminalModel
			{
				TerminalId = terminal.TerminalId,
				TerminalName = terminal.TerminalName,

			};

			return model;
		}
        [Authorize(Roles = "Admin")]
        public ActionResult DeleteTerminal(int terminalId)
        {
            try
            {

                var model = GetModel(terminalId);
                return View(model);
            }

            catch (Exception)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        [Authorize(Roles = "Admin")]
        public ActionResult DeleteTerminal(TerminalModel terminal)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _terminalManager.DeleteTerminal(terminal.TerminalId);
                    return RedirectToAction("Index");
                }
                else
                {
                    return HttpNotFound();
                }

            }

            catch (Exception)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }
    }
}