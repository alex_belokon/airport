﻿namespace MVC2.Models.Companies
{
    public class CityModel
    {
        public int CityId { get; set; }

        public string CityName { get; set; }
    }
}