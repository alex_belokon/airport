﻿namespace MVC2.Models.Companies
{
    public class CompanyContact
    {
        public int ContactId { get; set; }

        public string ContactName { get; set; }

        public string Telephone { get; set; }
    }
}