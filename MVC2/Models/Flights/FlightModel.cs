﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Airport.Core.Enums;
using Airport.Core.Models;
using MVC2.Models.Companies;

namespace MVC2.Models.Flights
{
    public class FlightModel
    {
		public int FlightId { get; set; }

	    [DisplayName("Flight number")]
		public string FlightNumber { get; set; }

	    [DisplayName("Flight date time")]
        [DisplayFormat(DataFormatString = "{0:s}", ApplyFormatInEditMode = true)]
        public DateTime FlightDateTime { get; set; }

	    [DisplayName("Flight duration")]
	    [DisplayFormat(DataFormatString = "{0:hh\\:mm}", ApplyFormatInEditMode = true)]
		public TimeSpan FlightDuration { get; set; }

	    public FlightStatusEnum FlightStatusId { get; set; }

	    //[DisplayName("City from")]
		public int CityFromId { get; set; }

	    //[DisplayName("City to")]
		public int CityToId { get; set; }

	    public int CompanyId { get; set; }

	    public int TerminalId { get; set; }

	    public int SelectedCityFromId { get; set; }

	    public int SelectedCityToId { get; set; }

	    public int SelectedCompanyId { get; set; }

	    public int SelectedTerminalId { get; set; }

		[DisplayName("City")]
		public List<City> Cities { get; set; }

		[DisplayName("Company")]
		public List<Company> Companies { get; set; }

	    [DisplayName("Terminal")]
		public List<Terminal> Terminals { get; set; }
	}
}