﻿namespace MVC2.Models.Home
{
    public class CustomRequest
    {
        public string Name { get; set; }
        public string Edu { get; set; }
        public bool? Married { get; set; }
    }
}