﻿namespace MVC2.Models.Terminals
{
    public class TerminalModel
    {
        public int TerminalId { get; set; }

        public string TerminalName { get; set; }
    }
}